import { useBody, useResponse, useState } from "@ki2/contextify-hooks";

import { wait, pickRandom } from "@ki2/utils";

const RAND_TIMES = [100, 200, 300]; // mean time = 200 ms

/** Action 1: code -> state
 *
 * Get a code from the client
 *
 */
export function actionCodeToState() {
  const body = useBody();
  const code = useState("code");

  code.value = body.code;

  console.log(`handle request ${code.value}`);
}

/** Action 2: wait
 *
 * Wait 100, 200 or 300 milliseconds (pick randomly)
 *
 */
export async function actionWait() {
  const waittime = useState("wait");
  waittime.value = pickRandom(RAND_TIMES);
  await wait(waittime.value);
}

/** Action 3: return code
 *
 * Return the code to the client
 */
export function actionReturnCode() {
  const code = useState("code");
  const waittime = useState("wait");

  console.log(
    `response to request ${code.value} (after waiting ${waittime.value} ms)`
  );

  useResponse({
    code: code.value,
  });
}
