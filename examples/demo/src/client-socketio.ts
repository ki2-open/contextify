import { nanoid } from "nanoid/non-secure";
import { io } from "socket.io-client";

const baseURL = "http://127.0.0.1:8080";

const socket = io(baseURL);

function emit(event: string, data: any): Promise<any> {
  return new Promise((resolve) => {
    socket.emit(event, data, (result: any) => {
      resolve(result);
    });
  });
}

async function main() {
  const gcode = nanoid();
  console.log("Generated code =", gcode);
  const response = await emit("code", {
    code: gcode,
  });
  console.log("Received code =", response.code);
}

main();
