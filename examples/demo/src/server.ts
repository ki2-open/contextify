import { express, middlewares } from "@ki2/contextify-express";
import { Server } from "@ki2/contextify-socketio";
import http from "node:http";

import { actionCodeToState, actionWait, actionReturnCode } from "./actions";

// base express setup
export const app = express();
export const server = http.createServer(app);
app.use(middlewares.json());

// base socketio setup
const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

// handle express route
app.xpost("/code", actionCodeToState, actionWait, actionReturnCode);

// handle socketio events
io.xon("connection", (socket) => {
  console.log(`new connection ${socket.id}`);

  socket.xon("code", actionCodeToState, actionWait, actionReturnCode);
});

server.listen(8080, () => {
  console.log("server listen on port 8080");
});
