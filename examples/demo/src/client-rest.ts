import { nanoid } from "nanoid/non-secure";
import fetch, { FetchError, Response } from "node-fetch";

const baseURL = "http://127.0.0.1:8080";

async function postJSON(url: string, body: any) {
  let response: Response;
  try {
    response = await fetch(url, {
      method: "post",
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
    });
    return await response.json();
  } catch (error) {
    console.log(error);
    if (error instanceof FetchError && error.type === "invalid-json") {
      console.log(response);
    }

    return undefined;
  }
}

async function main() {
  const gcode = nanoid();
  console.log("Generated code =", gcode);
  const response = await postJSON(`${baseURL}/code`, {
    code: gcode,
  });
  console.log("Received code =", response.code);
}

main();
