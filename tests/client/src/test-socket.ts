import { nanoid } from "nanoid/non-secure";
import { io } from "socket.io-client";
import { wait, pickRandom } from "./utils";

/** test-socket
 *
 *  This test run some socket-io api calls.
 *
 *  It use a globally defined socket : multiple events
 *  may be concatened by socket.io.
 *
 */

const baseURL = "http://127.0.0.1:8080";
const NB_ITER = 10;
//const RAND_TIMES = [50, 250, 600];
const RAND_TIMES = [0];

const socket = io(baseURL);

let countErrors: number = 0;

let requestTimes = [];

function emit(event: string, data: any): Promise<any> {
  return new Promise((resolve) => {
    socket.emit(event, data, (result: any) => {
      console.log(result?.code);
      resolve(result);
    });
  });
}

/** send
 *
 * Send random id to server and wait for the response containing
 * the same code.
 * It display an error in the console if the returned code is
 * not the same than the one sent.
 *
 * @param waitnb number of milliseconds to wait before sending
 *  the request
 */
async function send(waitnb: number) {
  const data = {
    code: nanoid(),
  };
  await wait(waitnb);
  const start = Date.now();
  const result = await emit("code", data);
  const duration = Date.now() - start;
  requestTimes.push(duration);
  console.log(duration);
  if (result?.code !== data.code) {
    console.error(`invalid code ${data.code} vs ${result?.code}`);
    countErrors++;
  }
}

async function main() {
  countErrors = 0;
  let p: Promise<any>[] = [];
  const start = Date.now();
  for (let i = 1; i <= NB_ITER; i++) {
    console.log(`send request ${i}/${NB_ITER}`);
    p.push(send(pickRandom(RAND_TIMES)));
  }
  console.log("wait for responses...");
  await Promise.all(p);
  const duration = Date.now() - start;
  if (countErrors > 0) {
    console.error(`WARNING: test ended with errors`);
  }
  console.log(`test run ${NB_ITER} iterations in ${duration} ms`);
  const durationByIter = duration / NB_ITER;
  console.log(`\tmeaning ${durationByIter} ms / iteration (average)`);
  const totalRequestDuration = requestTimes.reduce((pv, v) => pv + v, 0);
  const meanRequestDuration = totalRequestDuration / requestTimes.length;
  console.log(`mean request duration: ${meanRequestDuration} ms`);
  socket.close();
}

main();
