import { nanoid } from "nanoid/non-secure";
import { Manager } from "socket.io-client";
import { wait, pickRandom } from "./utils";

/** test-socket2
 *
 *  This test run some socket-io api calls.
 *
 *  It use a new socket manager at every call to avoid
 *  events concatenations.
 *
 */

const baseURL = "http://127.0.0.1:8080";
const NB_ITER = 100;
const RAND_TIMES = [50, 250, 600];
//const RAND_TIMES = [0];

let countErrors: number = 0;

//const socket = io(baseURL);

/** send
 *
 * Send random id to server and wait for the response containing
 * the same code.
 * It display an error in the console if the returned code is
 * not the same than the one sent.
 *
 * @param waitnb number of milliseconds to wait before sending
 *  the request
 */
async function send(waitnb: number) {
  const manager = new Manager(baseURL);
  const socket = manager.socket("/");
  const data = {
    code: nanoid(),
  };
  await wait(waitnb);
  socket.emit("code", data, (r: any) => {
    if (r?.code !== data.code) {
      console.error(`invalid code ${data.code} vs ${r?.code}`);
      countErrors++;
    }
    socket.close();
  });
}

for (let i = 1; i <= NB_ITER; i++) {
  console.log(`request ${i}/${NB_ITER}`);
  send(pickRandom([50, 250, 600]));
}

async function main() {
  countErrors = 0;
  let p: Promise<any>[] = [];
  const start = Date.now();
  for (let i = 1; i <= NB_ITER; i++) {
    console.log(`send request ${i}/${NB_ITER}`);
    p.push(send(pickRandom(RAND_TIMES)));
  }
  console.log("wait for responses...");
  await Promise.all(p);
  const duration = Date.now() - start;
  if (countErrors > 0) {
    console.error(`WARNING: test ended with errors`);
  }
  console.log(`test run ${NB_ITER} iterations in ${duration} ms`);
  const durationByIter = duration / NB_ITER;
  console.log(`\tmeaning ${durationByIter} ms / iteration (average)`);
}

main();
