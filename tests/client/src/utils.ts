import fetch, { FetchError, Response } from "node-fetch";

function wait(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

function pickRandom<T = any>(arr: T[]): T {
  const idx = getRandomInt(arr.length);
  return arr[idx];
}

async function postJSON(url: string, body: any, index: number = -1) {
  let response: Response;
  try {
    response = await fetch(url, {
      method: "post",
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
    });
    if (response.headers.get("content-type").includes("json")) {
      return await response.json();
    }

    const text = await response.text();

    console.warn(`WARNING: non JSON response.`);
    console.log(text);
  } catch (error) {
    console.error(`Error on index ${index}: ${error}`);
    if (error instanceof FetchError && error.type === "invalid-json") {
      console.log("HANDLE FETCH ERROR");
      console.log(response);
    }

    return undefined;
  }
}

export { wait, getRandomInt, pickRandom, postJSON };
