import { nanoid } from "nanoid/non-secure";
import { postJSON } from "./utils";
import { mean, std } from "mathjs";

/** test-rest
 *
 *  This test run some REST API calls.
 *
 */

const baseURL = "http://127.0.0.1:8080";
const NB_ITER = 1000;
let countErrors: number = 0;

let requestTimes = [];

/** send
 *
 * Send random id to server and wait for the response containing
 * the same code.
 * It display an error in the console if the returned code is
 * not the same than the one sent.
 *
 */
async function send() {
  const data = {
    code: nanoid(),
  };
  const response = await postJSON(`${baseURL}/code`, data);
  if (response?.code !== data.code) {
    console.error(`invalid code ${data.code} vs ${response?.code}`);
    countErrors++;
  }
  if (response) {
    requestTimes.push(response.duration);
  }
}

async function main() {
  countErrors = 0;
  requestTimes = [];
  console.log(`generate ${NB_ITER} requests`);
  const start = Date.now();
  for (let i = 1; i <= NB_ITER; i++) {
    console.log(`send request ${i}/${NB_ITER}`);
    await send();
  }
  const duration = Date.now() - start;

  if (countErrors > 0) {
    console.error(`WARNING: test ended with ${countErrors} errors`);
  }
  console.log(`test run ${NB_ITER} iterations in ${duration} ms`);

  const durationByIter = duration / NB_ITER;
  console.log(`\tmeaning ${durationByIter} ms / iteration (average)`);

  //const totalRequestDuration = requestTimes.reduce((pv, v) => pv + v, 0);
  //const meanRequestDuration = totalRequestDuration / requestTimes.length;
  const meanRequestDuration = mean(requestTimes);
  const stdRequestDuration = std(requestTimes);
  console.log(
    `mean request duration: ${meanRequestDuration} ± ${stdRequestDuration} ms`
  );
}

main();
