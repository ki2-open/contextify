import express from "express";
import http from "node:http";

import { createRoutes } from "./routes";

export const app = express();
export const server = http.createServer(app);
app.use(express.json());

createRoutes(app);

server.listen(8080, () => {
  console.log("server listen on port 8080");
});
