import type { Request, Response, NextFunction } from "express";
import { pickRandom, wait } from "./utils";

//const RAND_TIMES = [100, 200, 300]; // mean time = 200 ms
//const RAND_TIMES = [0, 10, 50]; // mean time = 20 ms
const RAND_TIMES = [100];

export function actionCodeToState(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const time = Date.now();
  const code = req.body.code;

  res.locals.time = time;

  console.log(`handle request ${code}`);
  next();
}

export async function actionWait(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const waittime = pickRandom(RAND_TIMES);
  res.locals.waittime = waittime;
  if (waittime > 0) {
    await wait(waittime);
  }
  next();
}

export function actionReturnCode(req: Request, res: Response) {
  const code = req.body.code;
  const waittime = res.locals.waittime;
  const time = res.locals.time;

  console.log(`response to request ${code} (after waiting ${waittime} ms)`);

  res.json({
    code,
    duration: Date.now() - time,
  });
}
