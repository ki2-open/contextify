export function wait(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function noop() {}

export function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

export function pickRandom<T = any>(arr: Array<T>): T {
  const idx = getRandomInt(arr.length);
  return arr[idx];
}
