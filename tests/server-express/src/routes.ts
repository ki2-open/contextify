import type { Express } from "express";

import { actionCodeToState, actionWait, actionReturnCode } from "./actions";

export function createRoutes(app: Express): Express {
  app.post("/code", actionCodeToState, actionWait, actionReturnCode);
  return app;
}
