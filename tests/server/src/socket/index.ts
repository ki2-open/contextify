import { Server } from "@ki2/contextify-socketio";

import { server } from "../rest";
import { createListeners } from "./listeners";

const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

io.xon("connection", (socket) => {
  createListeners(socket);
});
