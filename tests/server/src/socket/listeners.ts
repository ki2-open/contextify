import type { Socket } from "@ki2/contextify-socketio";

import { actionCodeToState, actionWait, actionReturnCode } from "../actions";

export function createListeners(socket: Socket) {
  console.log(`new connection ${socket.id}`);

  socket.xon("code", actionCodeToState, actionWait, actionReturnCode);
}
