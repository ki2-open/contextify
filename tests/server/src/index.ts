import { server } from "./rest";
import "./socket";

server.listen(8080, () => {
  console.log("server listen on port 8080");
});
