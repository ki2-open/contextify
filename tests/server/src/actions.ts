import { useBody, useResponse, useState } from "@ki2/contextify-hooks";

import { wait, pickRandom, getRandomInt } from "./utils";

//const RAND_TIMES = [100, 200, 300]; // mean time = 200 ms
const RAND_TIMES = [0, 10, 50]; // mean time = 20 ms
//const RAND_TIMES = [100];

function genRandomTime() {
  return 10 + getRandomInt(90);
}

function getWaitingTime(): number {
  return genRandomTime();
  //return pickRandom(RAND_TIMES);
}

export function actionCodeToState() {
  const start = Date.now();
  const body = useBody();
  const code = useState("code");
  const time = useState("time");

  time.set(start);

  code.value = body.code;

  console.log(`handle request ${code.value}`);
}

export async function actionWait() {
  const waittime = useState("wait");
  waittime.value = getWaitingTime();
  if (waittime.value > 0) {
    await wait(waittime.value);
  }
}

export function actionReturnCode() {
  const code = useState("code");
  const waittime = useState("wait");
  const time = useState("time");

  //await noop();

  console.log(
    `response to request ${code.value} (after waiting ${waittime.value} ms)`
  );

  useResponse({
    code: code.value,
    duration: Date.now() - time.value,
  });
}
