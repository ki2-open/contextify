//import express from "express";
import { express, middlewares } from "@ki2/contextify-express";
import http from "node:http";

import { createRoutes } from "./routes";

export const app = express();
export const server = http.createServer(app);
app.use(middlewares.json());

createRoutes(app);
