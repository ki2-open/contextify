//import type { Express } from "express";
import type { Express } from "@ki2/contextify-express";
import { contextify, Router } from "@ki2/contextify-express";

import { actionCodeToState, actionWait, actionReturnCode } from "../actions";

export function createRoutes(app: Express): Express {
  app.post(
    "/code",
    contextify(actionCodeToState, actionWait, actionReturnCode)
  );

  app.xpost("/code2", actionCodeToState, actionWait, actionReturnCode);

  const r = Router();

  r.xpost("/sub", actionCodeToState, actionWait, actionReturnCode);

  app.use("/code3", r);

  return app;
}
