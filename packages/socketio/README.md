# Contextify socket.io

This package give a `contextify` function to handle actions with `socket.io`

Usage :

```ts
// backend

function myAction() {
  await useSafe(() => wait(100)); // wait 100 ms

  useResponse({
    hello: "world",
  });
}

io.on("connection", (socket) => {
  socket.on(...contextify(socket, "my-event", myAction));
});

// frontend

socket.emit("my-event", (response) => {
  console.log(response);
  /** expected response :
   *
   * {
   *    hello: "world"
   * }
   *
   */
});
```

## Use socket context

A socket context is a context at the socket level (able to share data between events). When used, event reaction use this context as parent context.

```ts
// use socket context

io.on("connection", (socket) => {
  sctx = new SocketIoContext(socket);

  sctx.on("my-event", myAction);
});
```

In this example, the `my-event` event is handled in a unique context, but this context registers the socket context as parent to be able to manage shared information.
