import { exist } from "@ki2/utils";
import { errorToJSON, handleArgs, handleActions } from "@ki2/contextify-core";
import type { ContextifyArgs } from "@ki2/contextify-core";

import type { Socket } from "socket.io";

import {
  SocketIoHandlerContext,
  getDefaultSocketIoContextClass,
} from "./context";

type SocketIoCallbackArgs = Array<any> | [...Array<any>, (data: any) => void];

export function contextify(
  sockethandler: SocketIoHandlerContext | Socket,
  event: string,
  ...args: ContextifyArgs
): [string, (...args: SocketIoCallbackArgs) => void] {
  const { options, actions } = handleArgs(...args);

  let socket: Socket;
  if (sockethandler instanceof SocketIoHandlerContext) {
    socket = sockethandler.socket;
  } else {
    socket = sockethandler;
  }

  async function onCallback(...sargs: SocketIoCallbackArgs) {
    const last = sargs.pop();
    let callback: undefined | ((response: any) => void) = undefined;
    if (typeof last === "function") {
      callback = last;
    } else {
      sargs.push(last);
    }

    try {
      const CtxClass = getDefaultSocketIoContextClass();
      const ctx = new CtxClass(sockethandler);

      let body = sargs;
      if (sargs.length === 1) {
        body = sargs[0];
      }

      ctx.parameters.setParameters({
        $body: body,
        $query: event,
      });

      await handleActions(ctx, options, actions);

      const response = ctx.parameters.get("$response");
      if (exist(response)) {
        if (exist(callback)) {
          callback(response);
        } else {
          socket.emit(`${event}-response`, response);
        }
      }
    } catch (err) {
      const jerr = errorToJSON(err);
      socket.emit(`${event}-error`, jerr);
      if (exist(callback)) {
        callback(jerr);
      }
    }
  }

  return [event, onCallback];
}
