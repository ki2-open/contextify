import { nextTick } from "process";
import {
  Context,
  setSharedContext,
  removeSharedContext,
} from "@ki2/contextify-core";
import type { ActionFunction } from "@ki2/contextify-core";
import type { Socket } from "socket.io";

import { contextify } from "./contextify";

declare module "@ki2/contextify-core" {
  interface IParams {
    socket: Socket;
  }
}

export function generateSharedContextId(socket: Socket) {
  return "rt:socketio-" + String(socket.id);
}

/* TODO

set settings manager allowing to get socket object

*/

export class SocketIoHandlerContext extends Context {
  public socket: Socket;

  constructor(socket: Socket, autoregister: boolean = true) {
    super();
    this.socket = socket;
    this.setSource("rt", "socket.io");

    if (autoregister) {
      nextTick(() => {
        this.register();
      });
    }

    this.handleSocketioToContextify();
  }

  get sharedId(): string {
    return generateSharedContextId(this.socket);
  }

  register(): this {
    setSharedContext(this.sharedId, this);
    return this;
  }

  dispose(): void {
    removeSharedContext(this.sharedId);
  }

  on(event: string, ...actions: ActionFunction[]): this {
    this.socket.on(...contextify(this, event, ...actions));
    return this;
  }

  handleSocketioToContextify() {
    this.populateParameters();
    this.populateStates();
  }

  populateParameters(): void {
    this.parameters.setParameters({
      socket: this.socket,
    });
  }

  populateStates(): void {
    return;
  }
}

export class SocketIoContext extends Context {
  declare protected _parent: SocketIoHandlerContext | null;
  protected _socket: Socket | undefined;

  constructor(parentorsocket: SocketIoHandlerContext | Socket) {
    super();

    if (parentorsocket instanceof SocketIoHandlerContext) {
      this.setParent(parentorsocket);
    } else {
      this._socket = parentorsocket;
    }

    this.handleSocketRequestToContextify();
  }

  protected get socket() {
    return this._parent?.socket ?? this._socket!;
  }

  handleSocketRequestToContextify() {
    this.populateParameters();
    this.populateStates();
  }

  populateParameters(): void {
    this.parameters.setParameters({
      $ip: this.socket.handshake.address,
      $method: "event",
    });
  }

  populateStates(): void {
    return;
  }
}

export interface SocketIoContextConstructor<
  T extends SocketIoContext = SocketIoContext
> {
  new (parentorsocket: SocketIoHandlerContext | Socket): T;
}

let DEFAULT_SOCKETIO_CONTEXT_CLASS: SocketIoContextConstructor =
  SocketIoContext;

/** setDefaultSocketIoContextClass
 *
 * Change the default context used with the socket.io wrapper.
 *
 * @param contextclass New context class (reset to default if undefined)
 */
export function setDefaultSocketIoContextClass(
  contextclass?: SocketIoContextConstructor
) {
  DEFAULT_SOCKETIO_CONTEXT_CLASS = contextclass ?? SocketIoContext;
}

export function getDefaultSocketIoContextClass() {
  return DEFAULT_SOCKETIO_CONTEXT_CLASS;
}
