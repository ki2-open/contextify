import { Socket as OrigSocket } from "socket.io";
import type { ActionFunction } from "@ki2/contextify-core";
import { SocketIoHandlerContext } from "../wrapper";

export interface Socket extends OrigSocket {
  readonly ctx: SocketIoHandlerContext;
  xon(event: string, ...actions: ActionFunction[]): this;
}

function xon(this: Socket, event: string, ...actions: ActionFunction[]) {
  (this.ctx as SocketIoHandlerContext).on(event, ...actions);
  return this;
}

export function extendSocket(socket: OrigSocket): Socket {
  const ctx = new SocketIoHandlerContext(socket);
  Object.assign(socket, {
    ctx: ctx,
    xon: xon.bind(socket as Socket),
  });
  return socket as Socket;
}
