import { Server as OrigServer, Socket as OrigSocket } from "socket.io";

import { extendSocket } from "./Socket";

export type XServerCallBackFunction = (socket: any) => void;

export class Server extends OrigServer {
  xon(event: string, fn: XServerCallBackFunction) {
    super.on(event, (socket: OrigSocket) => {
      fn(extendSocket(socket));
    });
  }
}
