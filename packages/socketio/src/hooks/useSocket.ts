import { useContext, GeneralError } from "@ki2/contextify-core";
import { exist } from "@ki2/utils";
import type { Socket } from "socket.io";

export function useSocket(required: true): Socket;
export function useSocket(required: boolean): Socket | undefined;
export function useSocket(required: boolean = false): Socket | undefined {
  const ctx = useContext();
  const socket = ctx.parameters.get("$socket") as Socket | undefined;
  if (!exist(socket) && required) {
    throw new GeneralError("socket required");
  }
  return socket;
}
