import { exist } from "@ki2/utils";
import { useSocket } from "./useSocket";

export function useEmit<T = any>(event: string, required: boolean = false) {
  const socket = useSocket(required);
  if (exist(socket)) {
    return (data: T) => {
      socket.emit(event, data);
    };
  }
  return (data: T) => {};
}

export function useEmitWithReturn<T = any>(
  event: string,
  required: boolean = false
) {
  const socket = useSocket(required);
  if (exist(socket)) {
    return (data: T) => {
      return new Promise((resolve) => {
        socket.emit(event, data, resolve);
      });
    };
  }
  return (data: T) => {
    return Promise.resolve(undefined);
  };
}
