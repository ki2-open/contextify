# Contextify hooks

This package give hooks to handle the context.

## Response handling

### `useResponse()`

```ts
const response = useResponse();

// set the response through the value
response.value = {
  hello: "world",
};
```

### `useResponse(<response>)`

```ts
// set the response directly
useResponse({
  hello: "world",
});
```

## States handling

### `useState(<key>)`

```ts
const name = useState("name");

name.value = "John"; // set name state

useResponse({
  name: name.value, // get name state
});
```

### `useStateValue(<key>)`

```ts
// get the current "name" state value
const name = useStateValue("name");

setResponse({
  name,
});
```

## Parameters handling

### `useParam(<key>)`

```ts
const param = useParam("key");
```

### `useParam(<key>, <value>)`

```ts
useParam("key", "new value");
```

## Settings handling

### `useSetting(<key>)`

```ts
const option = useSetting("option");
```
