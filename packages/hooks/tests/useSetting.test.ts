import { useSetting } from "../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

describe("test useSetting hooks", () => {
  let ctx: Context | null = null;

  const data = 42;

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  // TODO: We need to implement a setting manager to test it
});
