import { useSource, useSourceEngine, useSourceType } from "../src";

import { Context, runInContext } from "@ki2/contextify-core";

import type {
  ContextSourceType,
  ContextSourceEngines,
  RestContextSourceEngines,
  RtContextSourceEngines,
} from "@ki2/contextify-core";

class TestContext extends Context {
  public setSource(): this;
  public setSource(type: null, engine: null): this;
  public setSource(type: string, engine: string): this;
  public setSource(type: "rest", engine: RestContextSourceEngines): this;
  public setSource(type: "rt", engine: RtContextSourceEngines): this;
  public setSource(
    type: ContextSourceType | null = null,
    engine: ContextSourceEngines | null = null
  ): this {
    return super.setSource(type as any, engine as any);
  }
}

describe("test useSource hooks", () => {
  it("should validate useSource", () => {
    const ctx = new TestContext().setSource("rest", "express");
    runInContext(ctx, () => {
      const source = useSource();
      expect(source.type).toBe("rest");
      expect(source.engine).toBe("express");
    });
  });

  it("should validate useSourceType", () => {
    const ctx = new TestContext().setSource("rt", "socket.io");
    runInContext(ctx, () => {
      expect(useSourceType()).toBe("rt");
    });
  });

  it("should validate useSourceEngine", () => {
    const ctx = new TestContext().setSource("rest", "express");
    runInContext(ctx, () => {
      expect(useSourceEngine()).toBe("express");
    });
  });

  it("should validate null source", () => {
    const ctx = new TestContext().setSource();
    runInContext(ctx, () => {
      expect(useSourceType()).toBe(null);
      expect(useSourceEngine()).toBe(null);
      const source = useSource();
      expect(source.type).toBe(null);
      expect(source.engine).toBe(null);
    });
  });
});
