import { useState, useStateValue } from "../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

describe("test useState hooks", () => {
  let ctx: Context | null = null;

  const data = "John";

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should get response object", () => {
    runInContext(ctx!, () => {
      const name = useState("name1");
      name.value = data;
      expect(name.value).toBe(data);
      expect(ctx!.states.getValue("name1")).toBe(data);
    });
  });

  it("should get value", () => {
    runInContext(ctx!, () => {
      ctx!.states.setValue("name2", data);
      const name = useStateValue("name2");
      expect(name).toBe(data);
    });
  });
});
