import { useQuery } from "../../src";
import type { IParamsKey } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

const __QUERY_KEY__: IParamsKey = "$query";

describe("test useQuery hooks", () => {
  let ctx: Context | null;

  const query = {
    some: "thing",
  };

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should correctly get query value", () => {
    runInContext(ctx!, () => {
      ctx!.parameters.set(__QUERY_KEY__, query);
      const lquery = useQuery();
      expect(lquery).toBe(query);
    });
  });
});
