import { useParam } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

describe("test useParams hooks", () => {
  let ctx: Context | null = null;

  const data = "value";

  const __TEST_KEY__ = "test";

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should correctly get parameter value", () => {
    runInContext(ctx!, () => {
      useParam(__TEST_KEY__, data);
      const p = useParam(__TEST_KEY__);
      expect(p).toBe(data);
    });
  });

  it("hould not throw error (read unsafe)", () => {
    runInContext(ctx!, () => {
      function run1() {
        useParam("$unsafe");
      }

      expect(run1).not.toThrow();
    });
  });

  it("should throw error (write unsafe)", () => {
    runInContext(ctx!, () => {
      function run1() {
        useParam("$unsafe", 12);
      }

      expect(run1).toThrow();
    });
  });
});
