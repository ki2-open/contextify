import { useId } from "../../src";
import type { IParamsKey } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

const __ID_KEY__: IParamsKey = "$id";

describe("test useId hooks", () => {
  let ctx: Context | null = null;

  const id = "123";

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should correctly get id value", () => {
    runInContext(ctx!, () => {
      ctx!.parameters.set(__ID_KEY__, id);
      const lid = useId();
      expect(lid).toBe(id);
    });
  });
});
