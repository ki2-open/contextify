import { useResponse } from "../../src";
import type { IParamsKey } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

const __RESPONSE_KEY__: IParamsKey = "$response";

describe("test useResponse hooks", () => {
  let ctx: Context | null;

  const data = { hello: "world" };

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should set response directly", () => {
    runInContext(ctx!, () => {
      useResponse(data);
      expect(ctx?.parameters.get(__RESPONSE_KEY__)).toBe(data);
    });
  });

  it("should correctly get response value", () => {
    runInContext(ctx!, () => {
      useResponse(data);
      const response = useResponse();
      expect(response).toBe(data);
    });
  });
});
