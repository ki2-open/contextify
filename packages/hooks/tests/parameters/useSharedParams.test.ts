import { useSharedParam } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

describe("test useParams hooks", () => {
  let ctx: Context | null;

  const data = "value";

  const __TEST_KEY__ = "test";

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should get parameter value", () => {
    runInContext(ctx!, () => {
      useSharedParam(__TEST_KEY__, data);
      const p = useSharedParam(__TEST_KEY__);
      expect(p).toBe(data);
    });
  });

  it("should not throw error (read unsafe)", () => {
    runInContext(ctx!, () => {
      function run1() {
        useSharedParam("$unsafe");
      }

      expect(run1).not.toThrow();
    });
  });

  it("should throw error (write unsafe)", () => {
    runInContext(ctx!, () => {
      function run1() {
        useSharedParam("$unsafe", 12);
      }

      expect(run1).toThrow();
    });
  });
});
