import { useBody } from "../../src";
import type { IParamsKey } from "../../src";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

const __BODY_KEY__: IParamsKey = "$body";

describe("test useBody hooks", () => {
  let ctx: Context | null = null;

  const body = {
    data: "entry",
  };

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should correctly get body", () => {
    runInContext(ctx!, () => {
      ctx!.parameters.set(__BODY_KEY__, body);
      const lbody = useBody();
      expect(lbody).toBe(body);
    });
  });
});
