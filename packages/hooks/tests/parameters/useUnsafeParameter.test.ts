import { useUnsafeParameter } from "../../src/parameters/unsafe";

import { Context, useContext, runInContext } from "@ki2/contextify-core";

describe("test useUnsafeParameter (internal) hooks", () => {
  let ctx: Context | null;

  const data = "value";

  const __TEST_KEY__ = "$test";

  beforeEach(() => {
    ctx = new Context();
  });

  afterEach(() => {
    ctx = null;
  });

  it("should validate setup", () => {
    runInContext(ctx!, () => {
      const loctx = useContext();
      expect(loctx).toBe(ctx);
    });
  });

  it("should get parameter value", () => {
    runInContext(ctx!, () => {
      ctx!.parameters.set(__TEST_KEY__, data);
      const p = useUnsafeParameter(__TEST_KEY__);
      expect(p).toBe(data);
    });
  });
});
