import { useContext } from "@ki2/contextify-core";

export function useSetting<T = any>(key: string) {
  return useContext().useSetting<T>(key);
}
