import { useSource } from ".";

export function useIsExpressSource() {
  const { type, engine } = useSource();
  return type === "rest" && engine === "express";
}
