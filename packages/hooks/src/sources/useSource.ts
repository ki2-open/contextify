import { useContext } from "@ki2/contextify-core";

export function useSource() {
  return useContext().source;
}

export function useSourceType() {
  return useSource().type;
}

export function useSourceEngine() {
  return useSource().engine;
}
