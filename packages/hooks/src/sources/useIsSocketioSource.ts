import { useSource } from ".";

export function useIsSocketioSource() {
  const { type, engine } = useSource();
  return type === "rt" && engine === "socket.io";
}
