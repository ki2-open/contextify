import { useContext } from "@ki2/contextify-core";
import { filterExist, isString } from "@ki2/utils";

type KeyItem = string | undefined | null;
type ArrayKey = Array<KeyItem>;
type KeyType = string | ArrayKey;

function genKey(key: KeyType) {
  if (isString(key)) {
    return key;
  }

  return filterExist(key).join("-");
}

export function useState<T = any>(key: KeyType) {
  return useContext().useState<T>(genKey(key));
}

export function useStateValue<T = any>(key: KeyType) {
  return useContext().useState<T>(genKey(key)).value;
}
