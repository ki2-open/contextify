import type { LiteralUnion } from "type-fest";

export interface IParams {
  $body: any;
  $id: string | number;
  $query: any;
  $response: any;
  $method: string;
  $route: string;
  $ip: string;
}

export type IParamsKey = LiteralUnion<keyof IParams, string>;
export type IParamsValue<K extends IParamsKey> = K extends keyof IParams
  ? IParams[K] | undefined
  : any | undefined;

export interface ISharedParams {}

export type ISharedKey = LiteralUnion<keyof ISharedParams, string>;
export type ISharedParamsKey = LiteralUnion<keyof ISharedParams, string>;
export type ISharedParamsValue<K extends ISharedParamsKey> =
  K extends keyof ISharedParams
    ? ISharedParams[K] | undefined
    : any | undefined;
