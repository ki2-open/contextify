import { useUnsafeParameter } from "./unsafe";

export function useMethod() {
  return useUnsafeParameter("$method");
}
