import { useUnsafeParameter } from "./unsafe";

export function useRoute() {
  return useUnsafeParameter("$route");
}
