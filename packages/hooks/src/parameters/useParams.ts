import { isUndefined } from "@ki2/utils";

import { checkSafeParameter } from "./utils";
import { useUnsafeParameter } from "./unsafe";
import type { IParamsKey, IParamsValue } from "./types";

export function useParam<K extends IParamsKey>(
  key: K
): IParamsValue<K> | undefined;
export function useParam<K extends IParamsKey>(
  key: K,
  value: IParamsValue<K>
): void;
export function useParam<K extends IParamsKey>(
  key: K,
  value?: IParamsValue<K>
): IParamsValue<K> | undefined | void;
export function useParam<K extends IParamsKey>(
  key: K,
  value?: IParamsValue<K>
) {
  if (!isUndefined(value)) {
    checkSafeParameter(key);
  }
  return useUnsafeParameter(key, value);
}
