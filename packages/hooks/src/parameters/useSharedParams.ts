import { isUndefined } from "@ki2/utils";

import { checkSafeParameter } from "./utils";
import { useUnsafeSharedParameter } from "./unsafeShared";
import type { ISharedParamsKey, ISharedParamsValue } from "./types";

export function useSharedParam<K extends ISharedParamsKey>(
  key: K
): ISharedParamsValue<K> | undefined;
export function useSharedParam<K extends ISharedParamsKey>(
  key: K,
  value: ISharedParamsValue<K>
): void;
export function useSharedParam<K extends ISharedParamsKey>(
  key: K,
  value?: ISharedParamsValue<K>
): ISharedParamsValue<K> | undefined | void;
export function useSharedParam<K extends ISharedParamsKey>(
  key: K,
  value?: ISharedParamsValue<K>
) {
  if (!isUndefined(value)) {
    checkSafeParameter(key);
  }
  return useUnsafeSharedParameter(key, value);
}
