import { exist } from "@ki2/utils";
import { useContext } from "@ki2/contextify-core";

import { useUnsafeParameter } from "./unsafe";
import type { IParamsValue } from "./types";

type TResponse = IParamsValue<"$response">;

export function useResponse(response: TResponse): void;
export function useResponse(): TResponse | undefined;
export function useResponse(
  response?: TResponse
): TResponse | undefined | void {
  return useUnsafeParameter("$response", response);
}
