import { useUnsafeParameter } from "./unsafe";

export function useId() {
  return useUnsafeParameter("$id");
}
