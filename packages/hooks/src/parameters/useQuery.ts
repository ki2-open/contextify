import { useUnsafeParameter } from "./unsafe";

export function useQuery() {
  return useUnsafeParameter("$query");
}
