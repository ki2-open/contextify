import { UnsafeError } from "@ki2/contextify-core";

export function checkSafeParameter(key: string) {
  if (key.startsWith("$")) {
    throw new UnsafeError("Invalid context parameters usage");
  }
}
