import { useContext } from "@ki2/contextify-core";
import type { ISharedParamsKey, ISharedParamsValue } from "./types";

export function useUnsafeSharedParameter<K extends ISharedParamsKey>(
  key: K
): ISharedParamsValue<K> | undefined;
export function useUnsafeSharedParameter<K extends ISharedParamsKey>(
  key: K,
  value: ISharedParamsValue<K>
): void;
export function useUnsafeSharedParameter<K extends ISharedParamsKey>(
  key: K,
  value?: ISharedParamsValue<K>
): ISharedParamsValue<K> | undefined | void;
export function useUnsafeSharedParameter<K extends ISharedParamsKey>(
  key: K,
  value?: ISharedParamsValue<K>
) {
  if (value === undefined) {
    return useContext().useSharedParameter<ISharedParamsValue<K>>(key);
  }
  useContext().useSetSharedParameter<ISharedParamsValue<K>>(key, value);
}
