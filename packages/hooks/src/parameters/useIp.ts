import { useUnsafeParameter } from "./unsafe";

export function useIp() {
  return useUnsafeParameter("$ip");
}
