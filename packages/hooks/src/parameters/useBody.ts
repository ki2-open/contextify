import { useUnsafeParameter } from "./unsafe";

export function useBody() {
  return useUnsafeParameter("$body");
}
