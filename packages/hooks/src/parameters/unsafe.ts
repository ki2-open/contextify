import { useContext } from "@ki2/contextify-core";
import type { IParamsKey, IParamsValue } from "./types";

export function useUnsafeParameter<K extends IParamsKey>(
  key: K
): IParamsValue<K> | undefined;
export function useUnsafeParameter<K extends IParamsKey>(
  key: K,
  value: IParamsValue<K>
): void;
export function useUnsafeParameter<K extends IParamsKey>(
  key: K,
  value?: IParamsValue<K>
): IParamsValue<K> | undefined | void;
export function useUnsafeParameter<K extends IParamsKey>(
  key: K,
  value?: IParamsValue<K>
) {
  if (value === undefined) {
    return useContext().useParameter<IParamsValue<K>>(key);
  }
  useContext().useSetParameter<IParamsValue<K>>(key, value);
}
