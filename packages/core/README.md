# Contextify Core

This package defines core concepts and methods to manage contexts.

_Remark :_ You probably don't need to use this package directly if you don't need to develop a wrapper (like the `express` one) for a new entry point.

The `contextify` framework is made around the concept of _context_. A _context_ object contains shared information. As the idea is to get different contexts in use in different situation. A _context handler_ is here to manage contexts switch. A default context handler is already set by default but can instantiate your own (but do this before any context was in use).
