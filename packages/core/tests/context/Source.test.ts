import { isContextSource } from "../../src";

describe("test Source handler", () => {
  it("should validate valid source", () => {
    expect(
      isContextSource({
        type: "rest",
        engine: "express",
      })
    ).toBe(true);
    expect(
      isContextSource({
        type: "rt",
        engine: "socket.io",
      })
    ).toBe(true);
    expect(
      isContextSource({
        type: null,
        engine: null,
      })
    ).toBe(true);
  });

  it("should validate invalid source", () => {
    expect(
      isContextSource({
        type: null,
        engine: "express",
      })
    ).toBe(false);
    expect(isContextSource({})).toBe(false);
    expect(isContextSource("test")).toBe(false);
  });
});
