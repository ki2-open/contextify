import { Context } from "../../src";

describe("test context", () => {
  it("should handle isChild property", () => {
    const ctx = new Context();
    const ctx2 = new Context(ctx);

    expect(ctx.isChild).toBe(false);
    expect(ctx2.isChild).toBe(true);
  });

  it("should get named context", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);
    const ctx3 = new Context(ctx2);

    ctx1.name = "ctx1";
    ctx2.name = "ctx2";
    ctx3.name = "ctx3";

    expect(ctx1.useNamedContext("ctx1")).toBe(ctx1);
    expect(ctx2.useNamedContext("ctx2")).toBe(ctx2);
    expect(ctx3.useNamedContext("ctx3")).toBe(ctx3);

    expect(ctx2.useNamedContext("ctx1")).toBe(ctx1);
    expect(ctx3.useNamedContext("ctx1")).toBe(ctx1);

    expect(ctx3.useNamedContext("ctx2")).toBe(ctx2);
  });

  it("should raise error for invalid named context", () => {
    const ctx = new Context();
    ctx.name = "ctx";

    expect(ctx.useNamedContext("ctx")).toBe(ctx);
    expect(() => ctx.useNamedContext("error")).toThrow(Error);
  });

  it("should get or create state", () => {
    const ctx = new Context();

    const state = ctx.useState("test");
    expect(state.value).toBe(undefined);
    state.set(42);
    const state2 = ctx.useState("test");
    expect(state2.value).toBe(42);
  });

  it("should get named context state", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);
    ctx1.name = "ctx1";
    ctx2.name = "ctx2";

    ctx1.useState("test").set(42);
    expect(ctx2.useNamedContextState("ctx1", "test"));
  });

  it("should get parameters", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);

    ctx1.parameters.set("a", 1);
    ctx2.parameters.set("b", 2);

    expect(ctx1.useParameter("a")).toBe(1);
    expect(ctx2.useParameter("b")).toBe(2);

    expect(ctx2.useParameter("a")).toBe(1);
  });

  it("should get named context parameter", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);
    ctx1.name = "ctx1";
    ctx1.parameters.set("test", 42);
    expect(ctx2.useNamedContextParameter("ctx1", "test")).toBe(42);
  });

  it("should get shared state", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);

    ctx1.stopSharedStates.add("test");
    const state1 = ctx2.useSharedState("test");
    state1.set(42);
    expect(ctx2.useSharedState("test")).toBe(state1);
    expect(ctx2.useSharedState("test").value).toBe(42);
    expect(ctx1.useSharedState("test").value).toBe(42);

    expect(ctx1.useState("test").value).toBe(42);
    expect(ctx1.states.has("test")).toBe(true);
    expect(ctx2.states.has("test")).toBe(false);
  });

  it("should get shared parameter", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);

    ctx1.stopSharedParameters.add("test");
    ctx1.parameters.set("test", 42);

    expect(ctx2.useSharedParameter("test")).toBe(42);
    expect(ctx2.useSharedParameter("test")).toBe(42);

    expect(ctx1.useParameter("test")).toBe(42);
    expect(ctx1.parameters.has("test")).toBe(true);
    expect(ctx2.parameters.has("test")).toBe(false);
  });

  it("should set correct parameter", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);

    ctx1.useSetParameter("a", 42);

    expect(ctx1.useParameter("a")).toBe(42);
    expect(ctx2.useParameter("a")).toBe(42);

    ctx2.useSetParameter("a", 24);

    expect(ctx1.useParameter("a")).toBe(42);
    expect(ctx2.useParameter("a")).toBe(24);
  });

  it("should set correct shared parameter", () => {
    const ctx1 = new Context();
    const ctx2 = new Context(ctx1);

    ctx1.stopSharedParameters.add("test");
    ctx2.useSetSharedParameter("test", 42);

    expect(ctx2.useSharedParameter("test")).toBe(42);
    expect(ctx1.parameters.get("test")).toBe(42);
    expect(ctx2.parameters.get("test")).toBe(undefined);
  });
});
