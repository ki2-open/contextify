import { ShareStopList } from "../../src";

describe("test ShareStopList class", () => {
  it("should not had items", () => {
    const list = new ShareStopList();
    expect(list.has("")).toBe(false);
  });

  it("should be able to add one item", () => {
    const list = new ShareStopList();
    list.add("test");
    expect(list.has("test")).toBe(true);
  });

  it("should be able to add many items", () => {
    const list = new ShareStopList();
    list.add("a", "b", "c");
    expect(list.has("a")).toBe(true);
    expect(list.has("b")).toBe(true);
    expect(list.has("c")).toBe(true);
  });

  it("should be able to add many items (array only)", () => {
    const list = new ShareStopList();
    list.add(["a", "b", "c"]);
    expect(list.has("a")).toBe(true);
    expect(list.has("b")).toBe(true);
    expect(list.has("c")).toBe(true);
  });

  it("should be able to add many items (mixed)", () => {
    const list = new ShareStopList();
    list.add("a", ["b", "c"]);
    expect(list.has("a")).toBe(true);
    expect(list.has("b")).toBe(true);
    expect(list.has("c")).toBe(true);
  });

  it("should be able to remove one item", () => {
    const list = new ShareStopList();
    list.add("a", "b", "c");
    list.remove("b");
    expect(list.has("a")).toBe(true);
    expect(list.has("b")).toBe(false);
    expect(list.has("c")).toBe(true);
  });

  it("should be able to remove many items", () => {
    const list = new ShareStopList();
    list.add("a", "b", "c");
    list.remove("a", "c");
    expect(list.has("a")).toBe(false);
    expect(list.has("b")).toBe(true);
    expect(list.has("c")).toBe(false);
  });

  it("should be able to remove many items (array)", () => {
    const list = new ShareStopList();
    list.add("a", "b", "c");
    list.remove(["a", "c"]);
    expect(list.has("a")).toBe(false);
    expect(list.has("b")).toBe(true);
    expect(list.has("c")).toBe(false);
  });

  it("should be able to remove many items (mixed)", () => {
    const list = new ShareStopList();
    list.add("a", "b", "c");
    list.remove(["a", "c"], "b");
    expect(list.has("a")).toBe(false);
    expect(list.has("b")).toBe(false);
    expect(list.has("c")).toBe(false);
  });
});
