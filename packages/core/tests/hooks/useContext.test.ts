import { useContext, Context, runInContext, GeneralError } from "../../src";

describe("test useContext hooks", () => {
  it("should raise exception", () => {
    expect(useContext).toThrow(GeneralError);
  });

  it("should return setted context", () => {
    const ctx = new Context();
    runInContext(ctx, () => {
      expect(useContext()).toBe(ctx);
    });
  });
});
