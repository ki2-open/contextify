import { LocalState } from "../../src";

describe("test local state", () => {
  it("should declare empty local state", () => {
    const state = new LocalState();

    expect(state).toBeInstanceOf(LocalState);
    expect(state.get()).toBe(undefined);
    expect(state.value).toBe(undefined);
    expect(state.isUndefined).toBe(true);
    expect(state.exist).toBe(false);
  });

  it("should set value correctly", () => {
    const state = new LocalState();

    state.set(5);

    expect(state.get()).toBe(5);
    expect(state.value).toBe(5);
    expect(state.exist).toBe(true);

    state.value = "test";
    expect(state.get()).toBe("test");
    expect(state.value).toBe("test");
    expect(state.exist).toBe(true);
  });

  it("should handle default value", () => {
    const state = new LocalState(10);

    expect(state.get()).toBe(10);
    expect(state.value).toBe(10);
    expect(state.exist).toBe(true);
  });

  it("should handle null correctly", () => {
    const state = new LocalState(null);

    expect(state.get()).toBe(null);
    expect(state.value).toBe(null);
    expect(state.isNull).toBe(true);
    expect(state.exist).toBe(false);
  });

  it("should handle clear correctly", () => {
    const state = new LocalState(5);

    expect(state.get()).toBe(5);
    expect(state.isUndefined).toBe(false);
    expect(state.exist).toBe(true);
    state.clear();
    expect(state.get()).toBe(undefined);
    expect(state.isUndefined).toBe(true);
    expect(state.exist).toBe(false);
  });
});
