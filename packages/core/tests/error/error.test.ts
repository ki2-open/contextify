import { ContextifyError, errorToJSON } from "../../src";

describe("test errors", () => {
  it("should contain setted content", () => {
    const err = new ContextifyError("code", "classname", "message");

    expect(err.name).toBe("ContextifyError");
    expect(err.code).toBe("code");
    expect(err.classname).toBe("classname");
    expect(err.message).toBe("message");
  });

  it("should generate valid json (from ContextifyError)", () => {
    const err = new ContextifyError("code", "classname", "message");

    const jerr = errorToJSON(err);

    expect(jerr).toStrictEqual({
      __onerror: true,
      message: "message",
      name: "ContextifyError",
      classname: "classname",
      code: "code",
    });
  });

  it("should generate valid json (from Error)", () => {
    const jerr = errorToJSON(new Error("message"));

    expect(jerr).toStrictEqual({
      __onerror: true,
      message: "message",
      name: "Error",
    });
  });
});
