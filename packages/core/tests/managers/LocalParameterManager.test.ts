import { LocalParametersManager } from "../../src";

describe("test LocalParameterManager class", () => {
  it("should correctly set & get parameter (base functions)", () => {
    const manager = new LocalParametersManager();
    manager.set("test", 42);
    expect(manager.has("test")).toBe(true);
    expect(manager.get("test")).toBe(42);
    expect(manager.keys()).toEqual(["test"]);
  });

  it("should set multiple parameters", () => {
    const manager = new LocalParametersManager();
    manager.setParameters({ test1: 42, test2: "42" });
    expect(manager.has("test1")).toBe(true);
    expect(manager.has("test2")).toBe(true);
    expect(manager.get("test1")).toBe(42);
    expect(manager.get("test2")).toBe("42");
    expect(manager.keys()).toEqual(["test1", "test2"]);
  });
});
