import { LocalStateManager, LocalState } from "../../src";

describe("test LocalStateManager class", () => {
  it("should correctly set & get state (base functions)", () => {
    const manager = new LocalStateManager();
    const state = new LocalState();
    manager.setState("test", state);

    expect(manager.has("test")).toBe(true);
    expect(manager.get("test")).toBe(state);
    expect(manager.keys()).toEqual(["test"]);
  });

  it("should create state", () => {
    const manager = new LocalStateManager();
    const state = manager.create("test");
    expect(manager.get("test")).toBe(state);
  });

  it("should get or create state", () => {
    const manager = new LocalStateManager();
    const state = manager.getOrCreate("test");
    expect(manager.get("test")).toBe(state);
    expect(manager.getOrCreate("test")).toBe(state);
  });

  it("should get value", () => {
    const manager = new LocalStateManager();
    const state = manager.create("test");
    state.set(42);
    expect(manager.getValue("test")).toBe(42);
  });

  it("should set value (setValue)", () => {
    const manager = new LocalStateManager();
    manager.setValue("test", 42);
    expect(manager.getValue("test")).toBe(42);
  });

  it("should set value (set)", () => {
    const manager = new LocalStateManager();
    manager.set("test", "42");
    expect(manager.getValue("test")).toBe("42");
  });
});
