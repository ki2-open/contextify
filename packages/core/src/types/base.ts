export type ActionFunction = () => void | Promise<void>;
