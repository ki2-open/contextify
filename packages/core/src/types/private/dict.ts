export interface IDictObject<T = any> {
  [key: string]: T;
}
