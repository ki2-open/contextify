export interface AbstractStateConstructor<
  T extends AbstractState = AbstractState
> {
  new (): T;
}

export abstract class AbstractState<T = any> {
  abstract get(): T | undefined;
  abstract set(v: T | undefined): void;

  constructor(defaultValue?: T | undefined) {
    this.set(defaultValue);
  }

  clear() {
    this.set(undefined);
  }

  get value(): T | undefined {
    return this.get();
  }

  set value(v: T | undefined) {
    this.set(v);
  }

  get isNull(): boolean {
    return this.get() === null;
  }

  get isUndefined(): boolean {
    return this.get() === undefined;
  }

  get exist(): boolean {
    return !this.isNull && !this.isUndefined;
  }
}

export class LocalState<T = any> extends AbstractState<T> {
  private _value: T | undefined;

  constructor(defaultValue?: T) {
    super(defaultValue);
  }

  get(): T | undefined {
    return this._value;
  }

  set(v: T | undefined): void {
    this._value = v;
  }
}
