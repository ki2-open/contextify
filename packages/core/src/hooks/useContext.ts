import { exist } from "@ki2/utils";
import { getAsyncLocalStorage } from "../context";
import type { Context } from "../context";
import { GeneralError } from "../errors";

export function useContext(): Context {
  const ctx = getAsyncLocalStorage().getStore();
  if (!exist(ctx)) {
    throw new GeneralError("[useContext] no context found.");
  }
  return ctx;
}
