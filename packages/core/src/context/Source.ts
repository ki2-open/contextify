import { hasKeys, isNull, isObject, isString } from "@ki2/utils";
import type { LiteralUnion } from "type-fest";

export type ContextSourceType = LiteralUnion<"rest" | "rt", string>;
export type RestContextSourceEngines = "express";
export type RtContextSourceEngines = "socket.io";

export type ContextSourceEngines = LiteralUnion<
  RestContextSourceEngines | RtContextSourceEngines,
  string
>;

export interface IContextSource {
  type: ContextSourceType | null;
  engine: ContextSourceEngines | null;
}

export function isContextSource(source: any): source is IContextSource {
  if (!isObject(source)) {
    return false;
  }

  if (!hasKeys(source, "type", "engine")) {
    return false;
  }

  if (isString(source.type) && isString(source.engine)) {
    return true;
  } else if (isNull(source.type) && isNull(source.engine)) {
    return true;
  }

  return false;
}
