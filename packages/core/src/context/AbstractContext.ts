import { exist, isNull } from "@ki2/utils";

import {
  AbstractState,
  AbstractStateManager,
  AbstractParameterManager,
  AbstractSettingsManager,
} from "..";

import { ShareStopList } from ".";
import type {
  IContextSource,
  ContextSourceType,
  ContextSourceEngines,
  RestContextSourceEngines,
  RtContextSourceEngines,
} from ".";

export interface IAbstractContextOptions {
  states: AbstractStateManager;
  parameters: AbstractParameterManager;
  settings: AbstractSettingsManager;
  parent?: AbstractContext;
}

export abstract class AbstractContext {
  protected _parent: AbstractContext | null = null;

  readonly stopSharedParameters: ShareStopList = new ShareStopList();
  readonly stopSharedStates: ShareStopList = new ShareStopList();

  protected _states: AbstractStateManager;
  protected _parameters: AbstractParameterManager;
  protected _settings: AbstractSettingsManager;

  protected _source: IContextSource | null = null;

  protected setSource(): this;
  protected setSource(type: null, engine: null): this;
  protected setSource(type: string, engine: string): this;
  protected setSource(type: "rest", engine: RestContextSourceEngines): this;
  protected setSource(type: "rt", engine: RtContextSourceEngines): this;
  protected setSource(
    type: ContextSourceType | null = null,
    engine: ContextSourceEngines | null = null
  ): this {
    this._source = {
      type,
      engine: engine as any,
    };
    return this;
  }

  public name: string | undefined;

  /**
   * @constructor
   * @param parent : parent context to create a sub-context (facultative)
   */
  constructor({
    parent,
    states,
    settings,
    parameters,
  }: IAbstractContextOptions) {
    if (exist(parent)) {
      this._parent = parent;
    }
    this._states = states;
    this._settings = settings;
    this._parameters = parameters;
  }

  public get isChild(): boolean {
    return exist(this._parent);
  }

  public get states() {
    return this._states;
  }

  public get parameters() {
    return this._parameters;
  }

  public get settings() {
    return this._settings;
  }

  public get source(): IContextSource {
    if (isNull(this._source)) {
      return {
        type: null,
        engine: null,
      };
    }
    return this._source;
  }

  public setParent(parent: AbstractContext) {
    this._parent = parent;
  }

  public useNamedContext(ctxname: string): AbstractContext {
    if (this.name === ctxname) {
      return this;
    }
    let ctx: AbstractContext | undefined =
      this._parent?.useNamedContext(ctxname);
    if (!exist(ctx)) {
      throw new Error(`No context named '${ctxname}' found.`);
    }
    return ctx;
  }

  public useSharedStateContext(key: string): AbstractContext {
    if (this.stopSharedStates.has(key) || !exist(this._parent)) {
      return this;
    }
    return this._parent.useSharedStateContext(key);
  }

  public useSharedParameterContext(key: string): AbstractContext {
    if (this.stopSharedParameters.has(key) || !exist(this._parent)) {
      return this;
    }
    return this._parent.useSharedParameterContext(key);
  }

  public useState<T = any>(key: string): AbstractState<T> {
    return this._states.getOrCreate(key);
  }

  public useNamedContextState<T = any>(ctxname: string, key: string) {
    return this.useNamedContext(ctxname).useState<T>(key);
  }

  public useSharedState<T = any>(key: string) {
    return this.useSharedStateContext(key).useState<T>(key);
  }

  public useParameter<T = any>(key: string): T | undefined {
    return this._parameters.get<T>(key) ?? this._parent?.useParameter<T>(key);
  }

  public useSetParameter<T = any>(key: string, value: T): void {
    this._parameters.set<T>(key, value);
  }

  public useNamedContextParameter<T = any>(ctxname: string, key: string) {
    return this.useNamedContext(ctxname).useParameter<T>(key);
  }

  public useSharedParameter<T = any>(key: string) {
    return this.useSharedParameterContext(key).useParameter<T>(key);
  }

  public useSetSharedParameter<T = any>(key: string, value: T) {
    return this.useSharedParameterContext(key).useSetParameter<T>(key, value);
  }

  public useSetting<T = any>(key: string): T | undefined {
    return this._settings.get<T>(key) ?? this._parent?.useSetting<T>(key);
  }
}
