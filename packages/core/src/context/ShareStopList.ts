export class ShareStopList {
  private data: string[] = [];

  protected addOne(key: string) {
    if (!this.has(key)) {
      this.data.push(key);
    }
  }

  protected addMany(keys: Array<string | string[]>) {
    for (const key of keys) {
      if (Array.isArray(key)) {
        this.addMany(key);
      } else {
        this.addOne(key);
      }
    }
  }

  protected removeOne(key: string) {
    this.data = this.data.filter((value) => value !== key);
  }

  protected removeMany(keys: Array<string | string[]>) {
    for (const key of keys) {
      if (Array.isArray(key)) {
        this.removeMany(key);
      } else {
        this.removeOne(key);
      }
    }
  }

  public has(key: string): boolean {
    return this.data.includes(key);
  }

  public add(...keys: Array<string | string[]>): this {
    this.addMany(keys);
    return this;
  }

  public remove(...keys: Array<string | string[]>): this {
    this.removeMany(keys);
    return this;
  }
}
