import { AsyncLocalStorage } from "node:async_hooks";
import type { Context } from "./context";

const asyncLocalStorage = new AsyncLocalStorage<Context>();

export function getAsyncLocalStorage(): AsyncLocalStorage<Context> {
  return asyncLocalStorage;
}

export function runInContext(ctx: Context, func: () => any) {
  return asyncLocalStorage.run(ctx, func);
}
