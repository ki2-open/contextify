import { AbstractContext } from ".";
import {
  LocalStateManager,
  LocalParametersManager,
  ConfigSettingsManager,
  IAbstractContextOptions,
} from "..";

export type ContextStateManagers = "$default" | "$settings" | "$parameters";

export type IContextOptions = Partial<IAbstractContextOptions>;

/** Represent a context shared between actions */
export class Context extends AbstractContext {
  /**
   * @constructor
   * @param parent : parent context to create a sub-context (facultative)
   */
  constructor(optOrParent: IContextOptions | Context = {}) {
    let options: IContextOptions = {};
    if (optOrParent instanceof AbstractContext) {
      options = {
        parent: optOrParent,
      };
    } else {
      options = optOrParent;
    }

    const fulloptions: IAbstractContextOptions = {
      parent: options.parent,
      states: options.states ?? new LocalStateManager(),
      parameters: options.parameters ?? new LocalParametersManager(),
      settings: options.settings ?? new ConfigSettingsManager(),
    };

    super(fulloptions);
  }
}
