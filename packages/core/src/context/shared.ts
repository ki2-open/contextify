import type { Context } from ".";

const __SHARED_CONTEXTS__ = new Map<string, Context>();

export function hasSharedContext(id: string) {
  return __SHARED_CONTEXTS__.has(id);
}

export function getSharedContext(id: string) {
  return __SHARED_CONTEXTS__.get(id);
}

export function setSharedContext(id: string, ctx: Context) {
  __SHARED_CONTEXTS__.set(id, ctx);
}

export function removeSharedContext(id: string) {
  return __SHARED_CONTEXTS__.delete(id);
}
