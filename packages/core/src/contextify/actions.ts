import { isPromise } from "@ki2/utils";

import type { ActionFunction } from "..";
import type { IFullContextifyOptions } from "./options";

import { UnsafeError } from "../errors";
import type { Context } from "../context";
import { getAsyncLocalStorage } from "../context";

export async function handleActions(
  ctx: Context,
  options: IFullContextifyOptions,
  actions: Array<ActionFunction>
) {
  const als = getAsyncLocalStorage();
  await als.run(ctx, async () => {
    for (const action of actions) {
      const p = action();
      if (isPromise(p)) {
        await p;
      }

      // if safe mode is activated
      if (options.safeMode !== false) {
        if (als.getStore() !== ctx) {
          let name = action.name ?? "";

          if (name.length === 0) {
            name = "[Anonymous function]";
          }

          if (options.safeMode === "warn") {
            options.warnLog(
              `Unsafe action '${name}' detected (context changed) and can lead to unexpected behavior inside it.`
            );
          } else {
            throw new UnsafeError(
              `Unsafe action '${name}' detected (context changed) and can lead to unexpected behavior inside it.`
            );
          }
        }
      }
    }
  });
}
