import { isFunction } from "@ki2/utils";

import { getOptions } from "./options";
import type { ActionFunction } from "..";
import type { IContextifyOptions } from "./options";

export type ContextifyArgs =
  | [...Array<ActionFunction>, IContextifyOptions]
  | Array<ActionFunction>;

export function isActionFunction(v: any): v is ActionFunction {
  return isFunction(v);
}

function throwActionsRequired() {
  throw Error("At least one action required.");
}

export function handleArgs(...args: ContextifyArgs) {
  if (args.length === 0) {
    throwActionsRequired();
  }

  let actions: Array<ActionFunction> = [];
  let options = getOptions();

  const last = args.pop();
  actions = args as Array<ActionFunction>;

  if (isActionFunction(last)) {
    actions.push(last);
  } else {
    options = getOptions(last);
  }

  if (actions.length === 0) {
    throwActionsRequired();
  }

  return {
    actions,
    options,
  };
}
