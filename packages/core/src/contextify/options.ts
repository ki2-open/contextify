export type SafeModeType = boolean | "warn";

export interface IFullContextifyOptions {
  safeMode: SafeModeType;
  warnLog: (msg: string) => void;
  errorLog: (msg: string) => void;
}

export type IContextifyOptions = Partial<IFullContextifyOptions>;

const __DEFAULT_OPTIONS__: IFullContextifyOptions = {
  safeMode: true,
  warnLog: console.warn,
  errorLog: console.error,
};

let current_default_options = __DEFAULT_OPTIONS__;

export function getDefaultOptions(): IFullContextifyOptions {
  return __DEFAULT_OPTIONS__;
}

export function getOptions(
  extend: IContextifyOptions = {}
): IFullContextifyOptions {
  return {
    ...current_default_options,
    ...extend,
  };
}

export function updateDefaultOptions(extend: IContextifyOptions) {
  current_default_options = {
    ...current_default_options,
    ...extend,
  };
}

export function resetDefaultOptions() {
  current_default_options = __DEFAULT_OPTIONS__;
}
