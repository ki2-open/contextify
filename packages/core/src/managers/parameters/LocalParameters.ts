import { AbstractParameterManager } from ".";

export class LocalParametersManager extends AbstractParameterManager {
  private parameters: Map<string, any> = new Map();

  public override get(key: string): any | undefined {
    return this.parameters.get(key);
  }

  public override set(key: string, parameter: any): void {
    this.parameters.set(key, parameter);
  }

  public override has(key: string): boolean {
    return this.parameters.has(key);
  }

  public override keys(): string[] {
    return Array.from(this.parameters.keys());
  }
}
