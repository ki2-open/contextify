import type { IDictObject } from "../../types/private";

export abstract class AbstractParameterManager<T = any> {
  public setParameters(parameters: IDictObject<T>): this {
    for (const key in parameters) {
      const value = parameters[key];
      this.set(key, value);
    }
    return this;
  }

  public abstract get<T = any>(key: string): T | undefined;
  public abstract set<T = any>(key: string, parameter: T): void;
  public abstract has(key: string): boolean;
  public abstract keys(): string[];
}
