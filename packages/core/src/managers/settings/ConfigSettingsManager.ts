import { AbstractSettingsManager } from ".";
process.env.SUPPRESS_NO_CONFIG_WARNING = "y";
import config from "config";

export class ConfigSettingsManager extends AbstractSettingsManager {
  override get<T = any>(key: string) {
    if (config.has(key)) {
      return config.get<T | undefined>(key);
    }
    return undefined;
  }
}
