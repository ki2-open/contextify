import { exist } from "@ki2/utils";

export abstract class AbstractSettingsManager {
  abstract get<T = any>(key: string): T | undefined;

  has(key: string) {
    return exist(this.get(key));
  }
}
