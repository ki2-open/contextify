export class StateError extends Error {}

export class AlreadyExistingStateError extends StateError {
  constructor(key: string) {
    super(`The state given by '${key}' already exist.`);
  }
}
export class NonExistingStateError extends StateError {
  constructor(key: string) {
    super(`The state given by '${key}' does not exist.`);
  }
}
