import { AbstractStateManager } from ".";
import { AbstractState } from "../..";

export class LocalStateManager<
  T extends AbstractState = AbstractState
> extends AbstractStateManager {
  protected states: Map<string, T> = new Map();

  public override get(key: string): AbstractState | undefined {
    return this.states.get(key) ?? undefined;
  }

  public override setState(key: string, state: T): void {
    this.states.set(key, state);
  }

  public override has(key: string): boolean {
    return this.states.has(key);
  }

  public override keys(): string[] {
    return Array.from(this.states.keys());
  }
}
