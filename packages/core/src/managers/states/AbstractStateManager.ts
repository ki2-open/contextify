import { AbstractState, AbstractStateConstructor, LocalState } from "../..";

export abstract class AbstractStateManager {
  protected _defaultStateGenerator: AbstractStateConstructor = LocalState;

  public setDefaultType<T extends AbstractState>(
    statetype: AbstractStateConstructor<T>
  ): this {
    this._defaultStateGenerator = statetype;
    return this;
  }

  public create(key: string, type?: AbstractStateConstructor): AbstractState {
    const generator = type ?? this._defaultStateGenerator;
    const state = new generator();
    this.set(key, state);
    return state;
  }

  public getOrCreate(key: string): AbstractState {
    return this.get(key) ?? this.create(key);
  }

  public set(key: string, stateOrValue: AbstractState | any): void {
    if (stateOrValue instanceof AbstractState) {
      this.setState(key, stateOrValue);
    } else {
      this.setValue(key, stateOrValue);
    }
  }

  public setValue(key: string, value: any): void {
    const state = this.getOrCreate(key);
    state.set(value);
  }

  public getValue(key: string): any {
    return this.get(key)?.value;
  }

  public abstract get(key: string): AbstractState | undefined;
  public abstract setState(key: string, state: AbstractState): void;
  public abstract has(key: string): boolean;
  public abstract keys(): string[];
}
