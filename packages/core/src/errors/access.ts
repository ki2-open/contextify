import { ContextifyError } from "./base";

export class BadRequestError extends ContextifyError {
  constructor(message?: string) {
    super("E2.0", "bad-request", message);
  }
}

export class ForbiddenError extends ContextifyError {
  constructor(message?: string) {
    super("E2.1", "forbidden", message);
  }
}

export class NotFoundError extends ContextifyError {
  constructor(message?: string) {
    super("E2.2", "not-found", message);
  }
}

export class MethodNotAllowedError extends ContextifyError {
  constructor(message?: string) {
    super("E2.3", "method-not-allowed", message);
  }
}
