import { ContextifyError } from "./base";

export interface IJsonError {
  __onerror: true;
  message?: string;
  name?: string;
  classname?: string;
  code?: string | number;
}

export function errorToJSON(error: any): IJsonError {
  let jsonerror: IJsonError = {
    __onerror: true,
  };

  if (error instanceof Error) {
    jsonerror = {
      ...jsonerror,
      message: error.message,
      name: error.name,
    };
  }

  if (error instanceof ContextifyError) {
    jsonerror = {
      ...jsonerror,
      classname: error.classname,
      code: error.code,
    };
  }

  return jsonerror;
}
