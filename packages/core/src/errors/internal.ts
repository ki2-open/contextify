import { ContextifyError } from "./base";

export class UnknownError extends ContextifyError {
  constructor(message?: string) {
    super("E0.0", "unknown", message);
  }
}

export class UnsafeError extends ContextifyError {
  constructor(message?: string) {
    super("E0.1", "unsafe", message);
  }
}

export class NoResponseError extends ContextifyError {
  constructor(message?: string) {
    super("E0.2", "no-response-set", message);
  }
}

export class GeneralError extends ContextifyError {
  constructor(message?: string) {
    super("E1.0", "general-error", message);
  }
}

export class NotImplementedError extends ContextifyError {
  constructor(message?: string) {
    super("E1.1", "not-implemented", message);
  }
}

export class BadGatewayError extends ContextifyError {
  constructor(message?: string) {
    super("E1.2", "bad-gateway", message);
  }
}

export class TimeoutError extends ContextifyError {
  constructor(message?: string) {
    super("E1.3", "timeout", message);
  }
}
