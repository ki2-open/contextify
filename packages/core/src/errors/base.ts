export class ContextifyError extends Error {
  code: string | number;
  classname: string;

  constructor(code: string | number, classname: string, message?: string) {
    super(message);
    this.code = code;
    this.classname = classname;
    this.name = "ContextifyError";
  }

  setCode(code: string | number): this {
    this.code = code;
    return this;
  }

  setClass(classname: string): this {
    this.classname = classname;
    return this;
  }

  setMessage(message: string): this {
    this.message = message;
    return this;
  }
}
