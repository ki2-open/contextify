# Contextify express

This package give a `contextify` function to handle actions with `express`

Usage :

```ts
// Simple exemple with external action

const app = express();

function myAction() {
  await wait(100); // wait 100 ms

  useResponse({
    hello: "world",
  });
}

app.get("/route", contextify(myAction));
```

```ts
// Example with multiple actions (in cascade) sharing the context.

const app = express();

app.get(
  "/route",
  contextify(
    () => {
      // first action that set a state
      const test = useState("test");
      test.value = "world";
    },
    () => {
      // second action using the previously set state
      const test = useState("test");

      useResponse({
        hello: test.value,
      });
    }
  )
);
```
