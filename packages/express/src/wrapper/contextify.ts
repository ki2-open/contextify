import { exist } from "@ki2/utils";

import {
  errorToJSON,
  NoResponseError,
  handleArgs,
  handleActions,
} from "@ki2/contextify-core";
import type { ContextifyArgs } from "@ki2/contextify-core";

import type { Request, Response, NextFunction } from "express";

import {
  AbstractExpressContext,
  getDefaultExpressContextClass,
  __CONTEXTIFY_CONTEXT_KEY__,
} from "./context";
import { errorToHttpCode } from "./httpcode";

export function contextify(...args: ContextifyArgs) {
  const { options, actions } = handleArgs(...args);

  return async (req: Request, res: Response, next?: NextFunction) => {
    let ctx: AbstractExpressContext;
    try {
      const resctx = res.locals[__CONTEXTIFY_CONTEXT_KEY__];
      if (exist(resctx) && resctx instanceof AbstractExpressContext) {
        // keep existing context if possible
        ctx = resctx;
        ctx.handleExpressToContextify();
      } else {
        // generate new context
        const CtxClass = getDefaultExpressContextClass();
        ctx = new CtxClass(req, res);
        res.locals[__CONTEXTIFY_CONTEXT_KEY__] = ctx;
      }

      await handleActions(ctx, options, actions);

      const response = ctx.useParameter("$response");

      if (exist(response)) {
        res.status(200).json(response);
      } else if (next) {
        ctx.handleContextifyToExpress();
        next();
      } else {
        throw new NoResponseError();
      }
    } catch (err) {
      const jerr = errorToJSON(err);
      const httpcode = errorToHttpCode(err);
      res.status(httpcode).json(jerr);

      options.errorLog(String(err));
    }
  };
}
