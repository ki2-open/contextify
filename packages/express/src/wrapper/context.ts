import { Context } from "@ki2/contextify-core";
import { isPromise } from "@ki2/utils";
import type { Request, Response } from "express";

import { AppSettingsManager } from "./AppSettingsManager";

declare module "@ki2/contextify-core" {
  interface IParams {
    authorization: string;
  }
}

export const __CONTEXTIFY_CONTEXT_KEY__ = "__contextify_context__";

export abstract class AbstractExpressContext extends Context {
  request: Request;
  response: Response;

  constructor(req: Request, res: Response) {
    super();
    this.setSource("rest", "express");
    this.request = req;
    this.response = res;

    this._settings = new AppSettingsManager(req.app);

    this.handleExpressToContextify();
  }

  protected get app() {
    return this.request.app;
  }

  // Handle things to do from express to contextify
  handleExpressToContextify() {
    this.populateParameters();
    this.populateStates();
  }

  // Handle things to do from contextify to express
  handleContextifyToExpress() {
    this.restoreRequest();
    this.restoreResponse();
  }

  abstract populateParameters(): void;
  abstract populateStates(): void;

  abstract restoreRequest(): void;
  abstract restoreResponse(): Promise<void>;
}

export function isContextifyContextKey(key: string): boolean {
  return key === __CONTEXTIFY_CONTEXT_KEY__;
}

export class ExpressContext extends AbstractExpressContext {
  override populateParameters(): void {
    const baseUrl = this.request.baseUrl;
    const path = this.request.path;
    let route: string = baseUrl;
    if (route.endsWith("/")) {
      route = route.slice(0, route.length - 1);
    }
    if (!path.startsWith("/")) {
      route += "/";
    }
    route += path;

    this.parameters.setParameters({
      $body: this.request.body,
      $query: this.request.query,
      $ip: this.request.ip,
      $method: this.request.method,
      $route: route,
      $id: this.request.params["id"],
      authorization: this.request.headers["authorization"],
    });
  }

  override populateStates(): void {
    const locals = this.response.locals;
    for (const key in locals) {
      if (isContextifyContextKey(key)) {
        continue;
      }
      this.states.set(key, locals[key]);
    }
  }

  override restoreRequest(): void {
    this.request.body = this.parameters.get("$body");
    this.request.query = this.parameters.get("$query") ?? {};
    //this.request.params["id"] = this.parameters.get("$id") ?? "";
  }

  override async restoreResponse(): Promise<void> {
    for (const key of this.states.keys()) {
      let state = this.states.getValue(key);
      if (isPromise(state)) {
        state = await state;
      }
      this.response.locals[key] = state;
    }
  }
}

export interface ExpressContextConstructor<
  T extends AbstractExpressContext = AbstractExpressContext
> {
  new (req: Request, res: Response): T;
}

let DEFAULT_EXPRESS_CONTEXT_CLASS: ExpressContextConstructor = ExpressContext;

/** setDefaultExpressContextClass
 *
 * Change the default context used with the express wrapper.
 *
 * @param contextclass New context class (reset to default if undefined)
 */
export function setDefaultExpressContextClass(
  contextclass?: ExpressContextConstructor
) {
  DEFAULT_EXPRESS_CONTEXT_CLASS = contextclass ?? ExpressContext;
}

export function getDefaultExpressContextClass() {
  return DEFAULT_EXPRESS_CONTEXT_CLASS;
}
