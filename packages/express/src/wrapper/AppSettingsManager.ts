import { ConfigSettingsManager } from "@ki2/contextify-core";

import type { Application } from "express";

export class AppSettingsManager extends ConfigSettingsManager {
  private app: Application;

  constructor(app: Application) {
    super();
    this.app = app;
  }

  override get<T = any>(key: string): T {
    return super.get<T>(key) ?? this.app.get(key);
  }
}
