import { ContextifyError } from "@ki2/contextify-core";

function contextifyErrorToHttpCode(err: ContextifyError): number {
  const code = String(err.code);

  if (code.startsWith("E0.")) {
    return 500;
  }

  if (code.startsWith("E1.")) {
    switch (code) {
      case "E1.0": // general-error
        return 500;
      case "E1.1": // not-implemented
        return 501;
      case "E1.2": // bad-gateway
        return 502;
      case "E1.3": // timeout
        return 503;
      default:
        return 500;
    }
  }

  if (code.startsWith("E2.")) {
    switch (code) {
      case "E2.0": // bad-request
        return 400;
      case "E2.1": // forbidden
        return 403;
      case "E2.2": // not-found
        return 404;
      case "E2.3": // method-not-allowed
        return 405;
      default:
        return 400;
    }
  }

  return 500;
}

export function errorToHttpCode(err: unknown): number {
  if (err instanceof ContextifyError) {
    return contextifyErrorToHttpCode(err);
  }

  return 500;
}
