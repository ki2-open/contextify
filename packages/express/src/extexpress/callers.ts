import { contextify } from "../wrapper";
import type { ContextifyArgs } from "@ki2/contextify-core";

import type { IRouter } from "express";

export function allCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.all(path, contextify(...ctxargs));
}

export function getCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.get(path, contextify(...ctxargs));
}

export function postCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.post(path, contextify(...ctxargs));
}

export function putCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.put(path, contextify(...ctxargs));
}

export function deleteCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.delete(path, contextify(...ctxargs));
}

export function patchCtx<Route extends string, T extends IRouter>(
  this: T,
  path: Route,
  ...ctxargs: ContextifyArgs
): T {
  return this.patch(path, contextify(...ctxargs));
}
