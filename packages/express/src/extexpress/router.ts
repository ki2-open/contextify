import { extproto } from "./base";
import type { IExtRouter } from "./base";

import e from "express";

import type { RouterOptions, Router as ExpressRouter } from "express";

export type Router = ExpressRouter & IExtRouter;

type RouterFct = (options?: RouterOptions) => Router;

Object.assign(e.Router, extproto);

export const Router = e.Router as RouterFct;
