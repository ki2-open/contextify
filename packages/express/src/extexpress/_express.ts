import { extproto } from "./base";
import type { IExtRouter } from "./base";

import e from "express";

export type Express = e.Express & IExtRouter;

Object.assign(e.application, extproto);

export type Application = e.Application & IExtRouter;

export function express(): Express {
  const app = e();
  Object.assign(app, extproto);
  return app as Express;
}
