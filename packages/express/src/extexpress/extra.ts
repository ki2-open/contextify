import e from "express";

export const middlewares = {
  json: e.json,
  raw: e.raw,
  text: e.text,
  static: e.static,
  urlencoded: e.urlencoded,
  query: e.query,
};
