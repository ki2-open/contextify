import type { ContextifyArgs } from "@ki2/contextify-core";

import * as c from "./callers";

interface IRouterMatcherCtx {
  <Route extends string>(path: Route, ...ctxargs: ContextifyArgs): void;
}

export interface IExtRouter {
  xall: IRouterMatcherCtx;
  xget: IRouterMatcherCtx;
  xpost: IRouterMatcherCtx;
  xput: IRouterMatcherCtx;
  xdelete: IRouterMatcherCtx;
  xpatch: IRouterMatcherCtx;
}

export const extproto: IExtRouter = {
  xall: c.allCtx,
  xget: c.getCtx,
  xpost: c.postCtx,
  xput: c.putCtx,
  xdelete: c.deleteCtx,
  xpatch: c.patchCtx,
};
