# Contextify

## Disclamer

The project is still in development and is not recommended for production.
If it's usable, it's not battle tested and the API is not stable yet. Some breaking change could appear before 1.0.0 version.

## Summary

This `contextify` module is made to execute a set of _actions_ in a specific context. This context carries essential information about the request and can be handled with _hooks_ (in a way similar to `React` hooks).

The aims are twofold: allow to defining _actions_ without dependencies to the transport (REST API, `socket.io`, `websocket`, etc.) and use a _React-like_ hooks system (in the sense that no context parameter is needed) that improve the code readability and testability.
It then simplifies code mutualization in project with multiple entry point or APIs.

```ts
// Example of custom action
function MyAction() {
  const name = useState("name");

  name.value = "world"; // set 'world' in the state named "name"

  // Do some things

  useResult({
    hello: name.value, // result send to the client
  });
}
```

```ts
// Example of custom action
async function MyAction() {
  const userId = useParam("userId"); // Example: request on /users/:userId

  // Do some things

  // Asynchronous call (example function)
  const user = await getUserFromDatabase(userId);

  // Do some other things with user

  // We suppose that the name state is set previously (e.g. in a previous action)
  const name = useState("name");

  useResult({
    hello: name.value, // result send to the client
  });
}
```

## Example

In the furthers examples, we will use these simple actions (`actions.ts`) :

```ts
const RAND_TIMES = [100, 200, 300]; // mean time = 200 ms

export function actionCodeToState() {
  // This action get the code sent by the client and handle it

  const body = useBody();
  const code = useState("code");

  code.value = body.code;

  console.log(`handle request ${code.value}`);
}

export async function actionWait() {
  // This action wait some time (set randomly from an array of possible values)

  const waittime = useState("wait");
  waittime.value = pickRandom(RAND_TIMES);
  await wait(waittime.value);
}

export function actionReturnCode() {
  // This action respond with the code sent by the client

  const code = useState("code");
  const waittime = useState("wait");

  console.log(
    `response to request ${code.value} (after waiting ${waittime.value} ms)`
  );

  useResponse({
    code: code.value,
  });
}
```

### With `express` API

Simple server configuration (`index.ts`) :

```ts
import type { Express } from "@ki2/contextify-express";
import {
  express,
  middlewares,
  contextify,
  Router,
} from "@ki2/contextify-express";
import http from "node:http";

import { actionCodeToState, actionWait, actionReturnCode } from "./actions";

const app = express();
const server = http.createServer(app);
app.use(middlewares.json());

// use standard express api with contextify wrapper
app.post("/code", contextify(actionCodeToState, actionWait, actionReturnCode));

// use extended express api handling contextify actions
app.xpost("/code2", actionCodeToState, actionWait, actionReturnCode);

// use Router + extended api
const r = Router();
r.xpost("/sub", actionCodeToState, actionWait, actionReturnCode);
app.use("/code3", r);

server.listen(8080, () => {
  console.log("server listen on port 8080");
});
```

### With `socketio` API

```ts
import { express, middlewares } from "@ki2/contextify-express";
import { SocketIoContext } from "@ki2/contextify-socketio";
import http from "node:http";
import { Server } from "socket.io";

import { actionCodeToState, actionWait, actionReturnCode } from "./actions";

export const app = express();
export const server = http.createServer(app);

io.on("connection", (socket) => {
  const sctx = new SocketIoContext(socket);
  console.log(`new connection ${socket.id}`);

  sctx.on("code", actionCodeToState, actionWait, actionReturnCode);
});
```

## Sub modules

The `contextify` module is splited in multiple submodules :

- `contextify-core` : module containing core definition for context & errors
- `contextify-hooks` : module containing base utilities hooks
- `contextify-express` : module defining function to handle _actions_ with `express`
- `contextify-socketio` : module defining function to handle _actions_ with `socket.io`
